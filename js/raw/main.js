$ = jQuery;

$(document).on('ready', ready);
$(document).on('scroll', scroll);
$(window).on('resize', resize);

function ready() {

    $('.popup').click(function() {

        var target = $(this).attr('href');
        $.magnificPopup.open({
            items: {
                src: target,
                type: 'inline'
            }
        });
    });
}

function scroll() {}

function resize() {}
